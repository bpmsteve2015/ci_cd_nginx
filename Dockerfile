#Node/Angular App ========================================
#FROM xujim/node-angular-cli-base:0.0.1 as node-angular-app
#LABEL authors="Jimmy Xu"

#ENV PATH /app/node_modules/.bin:$PATH


# Copy and install the Angular app
#COPY . /app
#RUN yarn install \
#  && ng build --prod --aot --build-optimizer

#Final nginx image ======================================
#FROM nginx:alpine
FROM dtr.dte.com/david.cao2/icon-nginx-2.2:base

MAINTAINER Jimmy Xu

# ENV BASE_URL http://localhost
ENV BASE_URL http://lph.icon.ehealthontario.ca
#ENV BASE_URL http://gbhu.icon.ehealthontario.ca
ENV ICON_SERVER http://icon_server_service
# Copy custom nginx config
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./nginx-startup.sh /
RUN chmod a+x /nginx-startup.sh

#COPY --from=node-angular-app /app/dist /usr/share/nginx/html
COPY ./dist/ /usr/share/nginx/html/

WORKDIR /usr/share/nginx/html

EXPOSE 80

# ENTRYPOINT ["nginx", "-g", "daemon off;"]
ENTRYPOINT ["/nginx-startup.sh"]