#!/bin/bash
# Author : Purna Poudel
ENV_NAME=$1;
PROP_FILE=$2;
awk -F: '/^[^#]/ {print $0}' $PROP_FILE | awk -vENV_NAME=$ENV_NAME 'BEGIN {FS="="} $1 ~ ENV_NAME {print $2}' | xargs