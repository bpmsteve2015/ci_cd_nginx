#!/bin/bash
# Script to query immunization records from ICON R2 to test application liveliness
# openssl x509 -inform der -in bmxd.cer -out bmxd.pem

# Immunization test case
OIID="${ICON_INTEGRATION_TEST_OIID}"
PIN="${ICON_INTEGRATION_TEST_PIN}"
CONTEXT="${ICON_INTEGRATION_TEST_CONTEXT}"

#HOST="https://xxsupportphu1.icon.ehealthontario.ca" # BMX-D Prod

#HOST_PORT="${1}"
HOST_PORT="${ICON_INTEGRATION_TEST_HOST}:${ICON_INTEGRATION_TEST_PORT}"
REAL_HOST_IP="${ICON_INTEGRATION_TEST_IP}"
PROTOCAL="${ICON_INTEGRATION_TEST_PROTOCAL}"

# Application constants
RESOLVE_STRING="--resolve ${HOST_PORT}:${REAL_HOST_IP}"
QUERY_URL="${PROTOCAL}://${HOST_PORT}/api/immunizationRetrieval"
#QUERY_URL="${HOST}/api/immunizationRetrieval"
echo "QUERY_URL ${QUERY_URL}"
SESSION_URL="${PROTOCAL}://${HOST_PORT}/api/token/session"
RETCODE=0 # Success

# Get ICON session token
echo curl -X  GET -v --insecure --silent -H "Content-Type: application/json" -H "Accept: application/json, text/plain, */*" $RESOLVE_STRING $SESSION_URL
SESSION_OUTPUT3=$(curl -X  GET -v --insecure --silent -H "Content-Type: application/json" -H "Accept: application/json, text/plain, */*" $RESOLVE_STRING $SESSION_URL 2>&1 )
echo "SESSION_OUTPUT3 ${SESSION_OUTPUT3}"
#SESSION_OUTPUT3=$(curl -X  GET -v --insecure   --silent   -H "Content-Type: application/json"   -H "Accept: application/json, text/plain, */*" -O temp.txt  $SESSION_URL 2>&1 )

#echo $SESSION_OUTPUT > token1.txt

SESSION_OUTPUT2=$(echo $SESSION_OUTPUT3 | awk -F"Authorization: " '{ print $2 }')
#echo $SESSION_OUTPUT2
#echo $SESSION_OUTPUT2 > token2.txt

SESSION_OUTPUT="token "$(echo $SESSION_OUTPUT2 | awk '{ print $1 }')
SESSION_TOKEN1=$(echo $SESSION_OUTPUT2 | awk '{ print $1 }')
# Extract session token from JSON output
echo $SESSION_OUTPUT


SESSION_TOKEN=""
HAS_TOKEN=`echo $SESSION_OUTPUT | awk '{print match($0, "token")}'`
if [ $HAS_TOKEN -gt 0 ]; then 
    SESSION_TOKEN=`echo $SESSION_OUTPUT | sed -e 's/[{}]/''/g' | awk -v RS=',"' -F: '/token/ {print $2}' | sed 's/\"//g'`
	
#    echo "DEBUG: Session=$SESSION_TOKEN"

else  
    echo "ERROR: Failed to get session token"  
    RETCODE=1 # Failure  
    exit $RETCODE
fi

# Retrieve immunization record

QUERY_OUTPUT=`curl -X GET -v --insecure --silent -H "session-token: $SESSION_TOKEN1"  -H "Content-Type: application/json" -H "Accept: application/json, text/plain, */*" -H "OIID: $OIID" -H "immunizations-context: $CONTEXT" ${RESOLVE_STRING} ${QUERY_URL}`   
   
    #echo ${QUERY_OUTPUT} > temp.json
    echo "session token:"  $SESSION_TOKEN1
#   echo "$QUERY_OUTPUT"
# Verify FHIR response contains resourceType
#echo $QUERY_OUTPUT # Debug

HAS_RESOURCE=`echo $QUERY_OUTPUT | awk '{print match($0, "resourceType")}'`

if [ $HAS_RESOURCE -gt 0 ]; then  
   echo "Query immunization SUCCESS"
else  
   echo "ERROR: Failed to get resource type" 
   echo "result return is:"   
   RETCODE=1 # Failure  
   exit $RETCODE
fi
  exit $RETCODE
