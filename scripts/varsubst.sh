#!/bin/bash
# Author : Purna Poudel
# Updated on August 22, 2018
# Accomodates ENV variables and variables passed through webhook & triggers.
templPath=$1;
propFile=$2;
function usage {
	echo "Usage: $0 <template path> <env.properties>";
}

if [[ $2 == "" ]]
    then
		echo "At least two arguments are required";
		usage;
		exit 1;
fi

# Create directory to stage generated files
if [[ -e ${templPath}/generated ]]; then
	rm -rf ${templPath}/generated;
	mkdir ${templPath}/generated;
else
	mkdir ${templPath}/generated;
fi
# Copy property file to generated directory.
propFileName="$(basename $propFile)";
cp ${propFile} ${templPath}/generated;

for _line in $(cat "${propFile}" | grep -v -e'#' | grep -v -e'^$'); do
   echo "Reading line: $_line from source property file: ${propFile}";
  _key=$(echo $_line | awk 'BEGIN {FS="="}{print $1}' | xargs);
	#echo "key:$_key";
  _value=$(eval echo $_line | cut -d '=' -f2 | xargs);
	#echo "val:$_value";
	# check if corresponding environment variable exists,
	# if so, replace the value in the property file
	if [[ ! -z "${_key}" && ! -z  ${!_key} ]]; then
		_nwkeyVal="${_key}=${!_key}";
		#echo "new key val: ${_nwkeyVal}";
		echo "Updating ${templPath}/generated/$propFileName";
		sed -i "s/^$_key=.*/${_nwkeyVal}/g" "${templPath}/generated/$propFileName"
		declare -x $_key=${!_key};
		#export ${_key}=${!_key};
	else
		declare -x $_key=$(echo $_value | xargs);	
		#export ${_key}=${!_key};
	fi	
done

for f in $(find $templPath -regex '.*\.y*ml'); do
  envsubst < $f > "${templPath}/generated/$(basename $f)";
done
